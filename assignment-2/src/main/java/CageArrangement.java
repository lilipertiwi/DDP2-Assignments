import animal.Animal;
import animal.Cat;
import animal.Lion;
import animal.Eagle;
import animal.Hamster;
import animal.Parrot;
import cage.IndoorCage;
import cage.OutdoorCage;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.String;

//method arrange di class indoor&outdoor dapat dibuat satu method utk menghindari redundant
//arraylist di method indoor di convert sebagai arraylist of animal
//method2 yg sama di tiap hewan dpt di override dari superclass Animal
//method call mengambil list of animal di main method

public class CageArrangement{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		String cats, lions, eagles, parrots, hamsters;
		ArrayList<Cat> cat = new ArrayList<Cat>();
		ArrayList<Lion> lion = new ArrayList<Lion>();
		ArrayList<Eagle> eagle = new ArrayList<Eagle>();
		ArrayList<Parrot> parrot = new ArrayList<Parrot>();
		ArrayList<Hamster> hamster = new ArrayList<Hamster>();
				
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");
		
		System.out.print("cat: ");
		String numOfCat = in.nextLine();
		if (Integer.parseInt(numOfCat) > 0){
			System.out.println("Provide the information of cat(s):");
			cats = in.nextLine();
			cat = arrangeCat(cats);
		}
		
		System.out.print("lion: ");
		String numOfLion = in.nextLine();
		if (Integer.parseInt(numOfLion) > 0){
			System.out.println("Provide the information of lion(s):");
			lions = in.nextLine();
			lion = arrangeLion(lions);
		}
		
		System.out.print("eagle: ");
		String numOfEagle = in.nextLine();
		if (Integer.parseInt(numOfEagle) > 0){
			System.out.println("Provide the information of eagle(s):");
			eagles = in.nextLine();
			eagle = arrangeEagle(eagles);
		}
		
		System.out.print("parrot: ");
		String numOfParrot = in.nextLine();
		if (Integer.parseInt(numOfParrot) > 0){
			System.out.println("Provide the information of parrot(s):");
			parrots = in.nextLine();
			parrot = arrangeParrot(parrots);
		}
		
		System.out.print("hamster: ");
		String numOfHamster = in.nextLine();
		if (Integer.parseInt(numOfHamster) > 0){
			System.out.println("Provide the information of hamster(s):");
			hamsters = in.nextLine();
			hamster = arrangeHamster(hamsters);
		}
		
		System.out.println("Animals have been successfully recorded!");
		System.out.println();
		System.out.println("=============================================");
		System.out.println();
		System.out.println("Cage arrangement:");
		
		IndoorCage.arrangeCat(cat);
		OutdoorCage.arrangeLion(lion);
		IndoorCage.arrangeParrot(parrot);
		OutdoorCage.arrangeEagle(eagle);
		IndoorCage.arrangeHamster(hamster);
		System.out.println();
		System.out.println();
		System.out.println("NUMBER OF ANIMAL:");
		System.out.println("cat: " + Cat.getTotalCats());
		System.out.println("lion: " + Lion.getTotalLions());
		System.out.println("parrot: " + Parrot.getTotalParrots());
		System.out.println("eagle: " + Eagle.getTotalEagles());
		System.out.println("hamster: " + Hamster.getTotalHamsters());
		System.out.println();
		System.out.println("=============================================");
		System.out.println();
		
		while (true){
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
			
			String pilihan = in.nextLine();
			if (pilihan.equals("1")){
				callCat(cat);
			}else if (pilihan.equals("2")){
				callEagle(eagle);
			}else if (pilihan.equals("3")){
				callHamster(hamster);
			}else if (pilihan.equals("4")){
				callParrot(parrot);
			}else if (pilihan.equals("5")){
				callLion(lion);
			}else if (pilihan.equals("99")){
				break;
			}
			else{
				System.out.println("You do nothing...");
			}
		}
	}
	
	//Create ArrayList of cats
	public static ArrayList<Cat> arrangeCat(String cats){
		ArrayList<Cat> cat = new ArrayList<Cat>();
		String[] kucing = cats.split(",");
		for (int i = 0; i < kucing.length; i++){
			String[] kcg = kucing[i].split("\\|");
			for (int j = 1; j < kcg.length; j++){
				Cat c = new Cat(kcg[j-1],Integer.parseInt(kcg[j]));
				cat.add(c);
			}
		}
		return cat;
	}
	
	//Create ArrayList of Lions
	public static ArrayList<Lion> arrangeLion(String lions){
		ArrayList<Lion> lion = new ArrayList<Lion>();
		String[] lionn = lions.split(",");		
		for (int i = 0; i < lionn.length; i++){
			String[] ln = lionn[i].split("\\|");
			for (int j = 1; j < ln.length; j++){
				Lion l = new Lion(ln[j-1],Integer.parseInt(ln[j]));
				lion.add(l);
			}
		}
		return lion;
	}
	
	//Create ArrayList of Eagles
	public static ArrayList<Eagle> arrangeEagle(String eagles){
		ArrayList<Eagle> eagle = new ArrayList<Eagle>();
		String[] elang = eagles.split(",");
		for (int i = 0; i < elang.length; i++){
			String[] elg = elang[i].split("\\|");
			for (int j = 1; j < elg.length; j++){
				Eagle e = new Eagle(elg[j-1],Integer.parseInt(elg[j]));
				eagle.add(e);
			}
		}
		return eagle;
	}
	
	//Create ArrayList of Parrots
	public static ArrayList<Parrot> arrangeParrot(String parrots){
		ArrayList<Parrot> parrot = new ArrayList<Parrot>();
		String[] burung = parrots.split(",");
		for (int i = 0; i < burung.length; i++){
			String[] brg = burung[i].split("\\|");
			for (int j = 1; j < brg.length; j++){
				Parrot p = new Parrot(brg[j-1],Integer.parseInt(brg[j]));
				parrot.add(p);
			}
			
		}
		return parrot;
	}
	
	//Create ArrayList of Hamsters
	public static ArrayList<Hamster> arrangeHamster(String hamsters){
		ArrayList<Hamster> hamster = new ArrayList<Hamster>();
		String[] hams = hamsters.split(",");
		for (int i = 0; i < hams.length; i++){
			String[] hms = hams[i].split("\\|");
			for (int j = 1; j < hms.length; j++){
				Hamster h = new Hamster(hms[j-1],Integer.parseInt(hms[j]));
				hamster.add(h);
			}
			
		}
		return hamster;
	}
	
	public static void callCat(ArrayList<Cat> cat){
		Scanner in = new Scanner(System.in);
		System.out.print("Mention the name of cat you want to visit: ");
		String name = in.nextLine();
		boolean catAda = false;
		for (Cat c : cat){
			if (c.getName().equals(name)){
				System.out.printf("You are visiting %s (cat) now, what would you like to do?", name);
				System.out.println();
				System.out.println("1: Brush the fur 2: Cuddle");
				String todo = in.nextLine();
				if (todo.equals("1")){
					System.out.println("Time to clean Katty's fur");
					System.out.print("Katty makes a voice: ");
					c.brushed();
				}else if (todo.equals("2")){
					System.out.print("Katty makes a voice: ");
					c.cuddled();
				}else{
					System.out.println("You do nothing...");
				}
				System.out.println("Back to the office!");
				catAda = true;
				break;
			}
			catAda = false;
		}
		if(catAda == false){
			System.out.print("There is no cat with that name! ");
			System.out.println("Back to the office!");
		}
		System.out.println();
	}
	
	public static void callParrot(ArrayList<Parrot> parrot){
		Scanner in = new Scanner(System.in);
		System.out.print("Mention the name of parrot you want to visit: ");
		String name = in.nextLine();
		boolean parrotAda = false;
		for (Parrot p : parrot){
			if (p.getName().equals(name)){
				System.out.printf("You are visiting %s (parrot) now, what would you like to do?", name);
				System.out.println();
				System.out.println("1: Order to fly 2: Do conversation");
				String todo = in.nextLine();
				if (todo.equals("1")){
					System.out.println("Parrot Greeny flies!");
					System.out.print(name +" makes a voice: ");
					p.fly();
				}else if (todo.equals("2")){
					System.out.print("You say: ");
					String say = in.nextLine();
					System.out.println(say);
					System.out.print(name +" says: ");
					p.imitate(say);
				}else{
					System.out.print(name +" says: ");
					p.mumble();
				}
				System.out.println("Back to the office!");
				parrotAda = true;
				break;
			}
			parrotAda = false;
		}
		if(parrotAda == false){
			System.out.print("There is no parrot with that name! ");
			System.out.println("Back to the office!");
		}
		System.out.println();
	}
	
	public static void callHamster(ArrayList<Hamster> hamster){
		Scanner in = new Scanner(System.in);
		System.out.print("Mention the name of hamster you want to visit: ");
		String name = in.nextLine();
		boolean hamsterAda = false;
		for (Hamster h : hamster){
			if (h.getName().equals(name)){
				System.out.printf("You are visiting %s (hamster) now, what would you like to do?", name);
				System.out.println();
				System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
				String todo = in.nextLine();
				if (todo.equals("1")){
					System.out.print(name +" makes a voice: ");
					h.gnaw();
				}else if (todo.equals("2")){
					System.out.print(name +" makes a voice: ");
					h.run();
				}else{
					System.out.println("You do nothing...");
				}
				System.out.println("Back to the office!");
				hamsterAda = true;
				break;
			}
			hamsterAda = false;
		}
		if(hamsterAda == false){
			System.out.print("There is no hamster with that name! ");
			System.out.println("Back to the office!");
		}
		System.out.println();
	}
	
	public static void callEagle(ArrayList<Eagle> eagle){
		Scanner in = new Scanner(System.in);
		System.out.print("Mention the name of eagle you want to visit: ");
		String name = in.nextLine();
		boolean eagleAda = false;
		for (Eagle e : eagle){
			if (e.getName().equals(name)){
				System.out.printf("You are visiting %s (eagle) now, what would you like to do?", name);
				System.out.println();
				System.out.println("1: Order to fly");
				String todo = in.nextLine();
				if (todo.equals("1")){
					System.out.print(name +" makes a voice: ");
					e.fly();
					System.out.println("You hurt!");
				}else{
					System.out.println("You do nothing...");
				}
				System.out.println("Back to the office!");
				eagleAda = true;
				break;
			}
			eagleAda = false;
		}
		if(eagleAda == false){
			System.out.print("There is no eagle with that name! ");
			System.out.println("Back to the office!");
		}
		System.out.println();
	}
	
	public static void callLion(ArrayList<Lion> lion){
		Scanner in = new Scanner(System.in);
		System.out.print("Mention the name of lion you want to visit: ");
		String name = in.nextLine();
		boolean lionAda = false;
		for (Lion l : lion){
			if (l.getName().equals(name)){
				System.out.printf("You are visiting %s (lion) now, what would you like to do?", name);
				System.out.println();
				System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
				String todo = in.nextLine();
				if (todo.equals("1")){
					System.out.println("Lion is hunting..");
					System.out.print(name + " makes a voice: ");
					l.hunt();
				}else if (todo.equals("2")){
					System.out.println("Clean the lion\\’s mane..");
					System.out.print(name + " makes a voice: ");
					l.brushed();
				}else if (todo.equals("3")){
					System.out.print(name + " makes a voice: ");
					l.disturbed();
				}else{
					System.out.println("You do nothing...");
				}
				System.out.println("Back to the office!");
				lionAda = true;
				break;
			}
			lionAda = false;
		}
		if(lionAda == false){
			System.out.print("There is no lion with that name! ");
			System.out.println("Back to the office!");
		}
		System.out.println();
	}
}