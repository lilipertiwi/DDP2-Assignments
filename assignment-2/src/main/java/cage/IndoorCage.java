package cage;
import animal.Cat;
import animal.Parrot;
import animal.Hamster;

import java.util.ArrayList;
import java.lang.String;

public class IndoorCage{
	
	public static void arrangeCat(ArrayList<Cat> cat){
		if (cat.size() != 0){
			System.out.println();
			System.out.println("location: " + Cat.getCage());
			int size = cat.size()/3; //size 3
			int pointer = cat.size()-1; //pointer 10
			if (cat.size() == 2){pointer = 1;}
			else if (cat.size() == 1){pointer = 0;}
			int level = 3;
			Cat[][] arr = new Cat[3][];					//cat.size = 10
			for (int i = 0; i < 3; i++) { 								//i<3		
				if (cat.size() >= 3){
					if (cat.size()%3 == 2 && (i == 0 || i == 1)){
						arr[i] = new Cat[size+1];
					}else if (cat.size()%3 == 1 && i == 0){
						arr[i] = new Cat[size+1];
					}else{
						arr[i] = new Cat[size];
					}
				}else if (cat.size() < 3){
					arr[i] = new Cat[1];
				}
				for (int j=0; j < arr[i].length; j++){ //j < 4
					if ((cat.size() == 2 && i == 0) || (cat.size() == 1 && (i == 0 || i == 1))){break;}
					arr[i][j] = cat.get(pointer); //cat.get(6)
					pointer--;
				}
				int k = arr[i].length-1;
				System.out.print("Level " + level +": ");
				for (int x=0; x < arr[i].length; x++){
					if ((cat.size() == 2 && i == 0) || (cat.size() == 1 && (i == 0 || i == 1))){break;}
					System.out.printf(arr[i][k].getName() + " " +"(%d - %c), ", arr[i][k].getLength(), arr[i][k].getSizeType());
					k--;
				}
				level--;
				System.out.println();
			}
			System.out.println();
			System.out.println("After rearrangement...");
			level = 3;
			for (int i = 1; i <= arr.length; i++){
				System.out.print("Level " + level +": ");
				if (i == 3){
					if (cat.size() < 3){break;}
					for (int j = 0; j < arr[0].length; j++){
						System.out.printf(arr[0][j].getName() + " " +"(%d - %c), ", arr[0][j].getLength(), arr[0][j].getSizeType());
					}
					break;
				}
				for (int j = 0; j < arr[i].length; j++){
					if(cat.size() == 1 && i == 1){
						System.out.printf(arr[2][j].getName() + " " +"(%d - %c), ", arr[2][j].getLength(), arr[2][j].getSizeType());
						break;
					}else if (cat.size() == 1 && i == 2 || i == 3){break;}
					System.out.printf(arr[i][j].getName() + " " +"(%d - %c), ", arr[i][j].getLength(), arr[i][j].getSizeType());
				}
				level--;
				System.out.println();
			}
			System.out.println();
		}
	}
	
	public static void arrangeHamster(ArrayList<Hamster> hamster){
		if (hamster.size() != 0){
			System.out.println();
			System.out.println("location: " + Hamster.getCage());
			int size = hamster.size()/3;
			int pointer = hamster.size()-1;
			if (hamster.size() == 2){pointer = 1;}
			else if (hamster.size() == 1){pointer = 0;}
			int level = 3;
			Hamster[][] arr = new Hamster[3][];				
			for (int i = 0; i < 3; i++) { 									
				if (hamster.size() >= 3){
					if (hamster.size()%3 == 2 && (i == 0 || i == 1)){
						arr[i] = new Hamster[size+1];
					}else if (hamster.size()%3 == 1 && i == 0){
						arr[i] = new Hamster[size+1];
					}else{
						arr[i] = new Hamster[size];
					}
				}else if (hamster.size() < 3){
					arr[i] = new Hamster[1];
				}
				for (int j=0; j < arr[i].length; j++){
					if ((hamster.size() == 2 && i == 0) || (hamster.size() == 1 && (i == 0 || i == 1))){break;}
					arr[i][j] = hamster.get(pointer);
					pointer--;
				}
				int k = arr[i].length-1;
				System.out.print("Level " + level +": ");
				for (int x=0; x < arr[i].length; x++){
					if ((hamster.size() == 2 && i == 0) || (hamster.size() == 1 && (i == 0 || i == 1))){break;}
					System.out.printf(arr[i][k].getName() + " " +"(%d - %c), ", arr[i][k].getLength(), arr[i][k].getSizeType());
					k--;
				}
				level--;
				System.out.println();
			}
			System.out.println();
			System.out.println("After rearrangement...");
			level = 3;
			for (int i = 1; i <= arr.length; i++){
				System.out.print("Level " + level +": ");
				if (i == 3){
					if (hamster.size() < 3){break;}
					for (int j = 0; j < arr[0].length; j++){
						System.out.printf(arr[0][j].getName() + " " +"(%d - %c), ", arr[0][j].getLength(), arr[0][j].getSizeType());
					}
					break;
				}
				for (int j = 0; j < arr[i].length; j++){
					if(hamster.size() == 1 && i == 1){
						System.out.printf(arr[2][j].getName() + " " +"(%d - %c), ", arr[2][j].getLength(), arr[2][j].getSizeType());
						break;
					}else if (hamster.size() == 1 && i == 2 || i == 3){break;}
					System.out.printf(arr[i][j].getName() + " " +"(%d - %c), ", arr[i][j].getLength(), arr[i][j].getSizeType());
				}
				level--;
				System.out.println();
			}
			System.out.println();
		}
	}
	
	public static void arrangeParrot(ArrayList<Parrot> parrot){
		if (parrot.size() != 0){
			System.out.println();
			System.out.println("location: " + Parrot.getCage());
			int size = parrot.size()/3;
			int pointer = parrot.size()-1;
			if (parrot.size() == 2){pointer = 1;}
			else if (parrot.size() == 1){pointer = 0;}
			int level = 3;
			Parrot[][] arr = new Parrot[3][];				
			for (int i = 0; i < 3; i++) { 									
				if (parrot.size() >= 3){
					if (parrot.size()%3 == 2 && (i == 0 || i == 1)){
						arr[i] = new Parrot[size+1];
					}else if (parrot.size()%3 == 1 && i == 0){
						arr[i] = new Parrot[size+1];
					}else{
						arr[i] = new Parrot[size];
					}
				}else if (parrot.size() < 3){
					arr[i] = new Parrot[1];
				}
				for (int j=0; j < arr[i].length; j++){
					if ((parrot.size() == 2 && i == 0) || (parrot.size() == 1 && (i == 0 || i == 1))){break;}
					arr[i][j] = parrot.get(pointer);
					pointer--;
				}
				int k = arr[i].length-1;
				System.out.print("Level " + level +": ");
				for (int x=0; x < arr[i].length; x++){
					if ((parrot.size() == 2 && i == 0) || (parrot.size() == 1 && (i == 0 || i == 1))){break;}
					System.out.printf(arr[i][k].getName() + " " +"(%d - %c), ", arr[i][k].getLength(), arr[i][k].getSizeType());
					k--;
				}
				level--;
				System.out.println();
			}
			System.out.println();
			System.out.println("After rearrangement...");
			level = 3;
			for (int i = 1; i <= arr.length; i++){
				System.out.print("Level " + level +": ");
				if (i == 3){
					if (parrot.size() < 3){break;}
					for (int j = 0; j < arr[0].length; j++){
						System.out.printf(arr[0][j].getName() + " " +"(%d - %c), ", arr[0][j].getLength(), arr[0][j].getSizeType());
					}
					break;
				}
				for (int j = 0; j < arr[i].length; j++){
					if(parrot.size() == 1 && i == 1){
						System.out.printf(arr[2][j].getName() + " " +"(%d - %c), ", arr[2][j].getLength(), arr[2][j].getSizeType());
						break;
					}else if (parrot.size() == 1 && i == 2 || i == 3){break;}
					System.out.printf(arr[i][j].getName() + " " +"(%d - %c), ", arr[i][j].getLength(), arr[i][j].getSizeType());
				}
				level--;
				System.out.println();
			}
			System.out.println();
		}
	}
}