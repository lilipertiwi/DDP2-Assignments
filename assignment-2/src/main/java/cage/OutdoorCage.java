package cage;
import animal.Lion;
import animal.Eagle;

import java.util.ArrayList;
import java.lang.String;

public class OutdoorCage{
	
	public static void arrangeEagle(ArrayList<Eagle> eagle){
		if (eagle.size() != 0){
			System.out.println();
			System.out.println("location: " + Eagle.getCage());
			int size = eagle.size()/3;
			int pointer = eagle.size()-1;
			if (eagle.size() == 2){pointer = 1;}
			else if (eagle.size() == 1){pointer = 0;}
			int level = 3;
			Eagle[][] arr = new Eagle[3][];				
			for (int i = 0; i < 3; i++) { 									
				if (eagle.size() >= 3){
					if (eagle.size()%3 == 2 && (i == 0 || i == 1)){
						arr[i] = new Eagle[size+1];
					}else if (eagle.size()%3 == 1 && i == 0){
						arr[i] = new Eagle[size+1];
					}else{
						arr[i] = new Eagle[size];
					}
				}else if (eagle.size() < 3){
					arr[i] = new Eagle[1];
				}
				for (int j=0; j < arr[i].length; j++){
					if ((eagle.size() == 2 && i == 0) || (eagle.size() == 1 && (i == 0 || i == 1))){break;}
					arr[i][j] = eagle.get(pointer);
					pointer--;
				}
				int k = arr[i].length-1;
				System.out.print("Level " + level +": ");
				for (int x=0; x < arr[i].length; x++){
					if ((eagle.size() == 2 && i == 0) || (eagle.size() == 1 && (i == 0 || i == 1))){break;}
					System.out.printf(arr[i][k].getName() + " " +"(%d - %c), ", arr[i][k].getLength(), arr[i][k].getSizeType());
					k--;
				}
				level--;
				System.out.println();
			}
			
			System.out.println();
			System.out.println("After rearrangement...");
			level = 3;
			for (int i = 1; i <= arr.length; i++){
				System.out.print("Level " + level +": ");
				if (i == 3){
					if (eagle.size() < 3){break;}
					for (int j = 0; j < arr[0].length; j++){
						System.out.printf(arr[0][j].getName() + " " +"(%d - %c), ", arr[0][j].getLength(), arr[0][j].getSizeType());
					}
					break;
				}
				for (int j = 0; j < arr[i].length; j++){
					if(eagle.size() == 1 && i == 1){
						System.out.printf(arr[2][j].getName() + " " +"(%d - %c), ", arr[2][j].getLength(), arr[2][j].getSizeType());
						break;
					}else if (eagle.size() == 1 && i == 2 || i == 3){break;}
					System.out.printf(arr[i][j].getName() + " " +"(%d - %c), ", arr[i][j].getLength(), arr[i][j].getSizeType());
				}
				level--;
				System.out.println();
			}
			System.out.println();
		}
	}
	
	public static void arrangeLion(ArrayList<Lion> lion){
		if (lion.size() != 0){
			System.out.println();
			System.out.println("location: " + Lion.getCage());
			int size = lion.size()/3;
			int pointer = lion.size()-1;
			if (lion.size() < 3){pointer = 1;}
			int level = 3;
			Lion[][] arr = new Lion[3][];				
			for (int i = 0; i < 3; i++) { 									
				if (lion.size() >= 3){
					if (lion.size()%3 == 2 && (i == 0 || i == 1)){
						arr[i] = new Lion[size+1];
					}else if (lion.size()%3 == 1 && i == 0){
						arr[i] = new Lion[size+1];
					}else{
						arr[i] = new Lion[size];
					}
				}else if (lion.size() < 3){
					arr[i] = new Lion[1];
				}
				for (int j=0; j < arr[i].length; j++){
					if ((lion.size() == 2 && i == 0) || (lion.size() == 1 && (i == 0 || i == 1))){break;}
					arr[i][j] = lion.get(pointer);
					pointer--;
				}
				int k = arr[i].length-1;
				System.out.print("Level " + level +": ");
				for (int x=0; x < arr[i].length; x++){
					if ((lion.size() == 2 && i == 0) || (lion.size() == 1 && (i == 0 || i == 1))){break;}
					System.out.printf(arr[i][k].getName() + " " +"(%d - %c), ", arr[i][k].getLength(), arr[i][k].getSizeType());
					k--;
				}
				level--;
				System.out.println();
			}
			System.out.println();
			System.out.println("After rearrangement...");
			level = 3;
			for (int i = 1; i <= arr.length; i++){
				System.out.print("Level " + level +": ");
				if (i == 3){
					if (lion.size() < 3){break;}
					for (int j = 0; j < arr[0].length; j++){
						System.out.printf(arr[0][j].getName() + " " +"(%d - %c), ", arr[0][j].getLength(), arr[0][j].getSizeType());
					}
					break;
				}
				for (int j = 0; j < arr[i].length; j++){
					if(lion.size() == 1 && i == 1){
						System.out.printf(arr[2][j].getName() + " " +"(%d - %c), ", arr[2][j].getLength(), arr[2][j].getSizeType());
						break;
					}else if (lion.size() == 1 && i == 2 || i == 3){break;}
					System.out.printf(arr[i][j].getName() + " " +"(%d - %c), ", arr[i][j].getLength(), arr[i][j].getSizeType());
				}
				level--;
				System.out.println();
			}
			System.out.println();
		}
	}
}