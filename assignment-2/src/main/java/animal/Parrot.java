package animal;

public class Parrot extends Animal{
	private static int totalParrots;
	private static String cage = "indoor";
	private char sizeType;
	
	public Parrot(String name, int length){
		super(name, length);
		totalParrots++;
	}
	
	public static int getTotalParrots(){
		return totalParrots;
	}
	
	public void setTotalParrots(int nums){
		this.totalParrots = nums;
	}
	
	public static String getCage(){
		return cage;
	}
	
	public char getSizeType(){
		if (getLength() < 45){
			sizeType = 'A';
		}else if (getLength() >= 45 && getLength() < 60){
			sizeType = 'B';
		}else if (getLength() >= 60){
			sizeType = 'C';
		}
		return sizeType;
	}
	
	public void fly(){
		System.out.println("TERBAAANG...");
	}
	
	public void mumble(){
		System.out.println("HM?");
	}
	
	public void imitate(String words){
		System.out.println(words.toUpperCase());
	}
}