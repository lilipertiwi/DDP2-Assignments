package animal;
import java.util.Random;

public class Cat extends Animal{
	private static int totalCats;
	private static String cage = "indoor";
	private char sizeType;
	
	public Cat(String name, int length){
		super(name, length);
		totalCats++;
	}
	
	public static int getTotalCats(){
		return totalCats;
	}
	
	public void setTotalCats(int nums){
		this.totalCats = nums;
	}
	
	public static String getCage(){
		return cage;
	}
	
	public char getSizeType(){
		if (getLength() < 45){
			sizeType = 'A';
		}else if ((getLength() >= 45) && (getLength() < 60)){
			sizeType = 'B';
		}else if (getLength() >= 60){
			sizeType = 'C';
		}
		return sizeType;
	}
	
	public void brushed(){
		System.out.println("Nyaaan...");
	}
	
	public void cuddled(){
		Random rand = new Random();
		String[] miaw = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
		int index = rand.nextInt(4);
		System.out.println(miaw[index]);
	}
}