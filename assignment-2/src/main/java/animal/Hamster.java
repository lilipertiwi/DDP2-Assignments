package animal;

public class Hamster extends Animal{
	private static int totalHamsters;
	private static String cage = "indoor";
	private char sizeType;
	
	public Hamster(String name, int length){
		super(name, length);
		totalHamsters++;
	}
	
	public static int getTotalHamsters(){
		return totalHamsters;
	}
	
	public void setTotalHamsters(int nums){
		this.totalHamsters = nums;
	}
	
	public static String getCage(){
		return cage;
	}
	
	public char getSizeType(){
		if (getLength() < 45){
			sizeType = 'A';
		}else if (getLength() >= 45 && getLength() < 60){
			sizeType = 'B';
		}else if (getLength() >= 60){
			sizeType = 'C';
		}
		return sizeType;
	}
	
	public void gnaw(){
		System.out.println("Ngkkrit.. Ngkkrrriiit");
	}
	
	public void run(){
		System.out.println("Trrr... Trrr...");
	}
}