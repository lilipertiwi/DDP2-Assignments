package animal;

public class Lion extends Animal{
	private static int totalLions;
	private static String cage = "outdoor";
	private char sizeType;
	
	public Lion(String name, int length){
		super(name, length);
		totalLions++;
	}
	
	public static int getTotalLions(){
		return totalLions;
	}
	
	public void setTotalLions(int nums){
		this.totalLions = nums;
	}
	
	public static String getCage(){
		return cage;
	}
	
	public char getSizeType(){
		if (getLength() < 75){
			sizeType = 'A';
		}else if (getLength() >= 75 && getLength() < 90){
			sizeType = 'B';
		}else if (getLength() >= 90){
			sizeType = 'C';
		}
		return sizeType;
	}
	
	public void hunt(){
		System.out.println("Err...!");
	}
	
	public void brushed(){
		System.out.println("Hauhhmm!");
	}
	
	public void disturbed(){
		System.out.println("HAAHUM!!");
	}
}