package animal;

public class Animal{
	private String name;
	private int length;
	
	public Animal(String name, int length){
		this.name = name;
		this.length = length;
	}
	
	public String getName(){
		return name;
	}
	
	public int getLength(){
		return length;
	}
	
	//public getTotalAnimal{ ... } >> buat override method
}