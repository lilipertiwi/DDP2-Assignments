package animal;

public class Eagle extends Animal{
	private static int totalEagles;
	private static String cage = "outdoor";
	private char sizeType;
	
	public Eagle(String name, int length){
		super(name, length);
		totalEagles++;
	}
	
	public static int getTotalEagles(){
		return totalEagles;
	}
	
	public void setTotalEagles(int nums){
		this.totalEagles = nums;
	}
	
	public static String getCage(){
		return cage;
	}
	
	public char getSizeType(){
		if (getLength() < 75){
			sizeType = 'A';
		}else if (getLength() >= 75 && getLength() < 90){
			sizeType = 'B';
		}else if (getLength() >= 90){
			sizeType = 'C';
		}
		return sizeType;
	}
	
	public void fly(){
		System.out.println("Kwaakk....");
	}
}