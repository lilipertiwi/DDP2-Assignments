import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
		TrainCar train = new TrainCar();
		Scanner input = new Scanner(System.in);
		 
		int num = Integer.parseInt(input.nextLine());
		for(int i = 0; i < num; i++){
			String inp = input.nextLine();
			String cat[] = inp.split(",");
			String catName = cat[0];
			double catWeight = Double.parseDouble(cat[1]);
			double catHeight = Double.parseDouble(cat[2]);
			WildCat kucing = new WildCat(catName, catWeight, catHeight);
			if(i == 0){
                train = new TrainCar(kucing);
            } else {
                if (train.computeTotalWeight() + kucing.getWeight()<= THRESHOLD){
                    train = new TrainCar(kucing, train);
                } else {     
                    train.printCar();
                    train = new TrainCar(kucing);
                }
                
            }
			
		}
		train.printCar();
		
    }
	
}
