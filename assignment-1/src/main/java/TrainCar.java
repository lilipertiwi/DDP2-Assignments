public class TrainCar {
	// TODO Complete me!
    public static final double EMPTY_WEIGHT = 20; // In kilograms
	private WildCat cat;
	private TrainCar next;
    

    public TrainCar(WildCat cat) {
        // TODO Complete me!
		this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
		this.cat = cat;
		this.next = next;
    }
	
	public TrainCar (){
       
   }

    public double computeTotalWeight() {
        // TODO Complete me!
		double totalWeight;
		if(next != null){
			totalWeight = cat.getWeight() + next.computeTotalWeight();
		}else{
			totalWeight = cat.getWeight() + EMPTY_WEIGHT;
		}
        return totalWeight;
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
		double totalMassIndex;
		if(next != null){
			totalMassIndex = cat.computeMassIndex() + next.computeTotalMassIndex();
		}else{
			totalMassIndex = cat.computeMassIndex();
		}
        return totalMassIndex;
    }

    public void printCar() {
        // TODO Complete me!
		System.out.println("The train departs to Javari Park");
		System.out.println("[LOCO]<--"+this.getAllCatName());
		double avg = this.computeTotalMassIndex()/this.getAllNumTrain();
		System.out.println("Average mass index of all cats :" + String.format("%.2f", avg));
		System.out.println("In average, the cats in the train are"+"*"+ categories(avg)+"*");
		
    }
	
	public static String categories(double average){
       if(average < 18.5){
           return "underweight";
       }
       else if(average <= 25){
           return "normal";
       }
       else if (average >= 25 && average <= 30){
           return "overweight";
      }
       else{
       return "obses";
		}
	}
  
   
   public String getAllCatName(){ 
       String allCats = "";
       if(next == null){
           allCats = "("+cat.getName()+")";
       }else{
           allCats = "("+cat.getName()+")"+"--"+next.getAllCatName();
       }
       
       return allCats;
   }
      public int getAllNumTrain(){ 
       int totalTrain = 0;
       if(next != null){
            totalTrain = 1+next.getAllNumTrain();
        }else{
           totalTrain = 1;
       }
       return totalTrain;
   }
}
