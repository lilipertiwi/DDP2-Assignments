import java.lang.Math;

public class WildCat {

    // TODO Complete me!
    private String name;
    private double weight; // In kilograms
    private double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
		this.name = name;
		this.weight = weight;
		this.length = length;
    }
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public double getWeight(){
		return weight;
	}
	
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	public double getLength(){
		return length;
	}
	
	public void setLength(double length){
		this.length = length;
	}

    public double computeMassIndex() {
        // TODO Complete me!
		double catLgth = getLength()/100;
		double BMI = getWeight() / (Math.pow(catLgth,2));
		double roundBMI = Math.round(BMI * 100.0)/100.0;
        return roundBMI;
    }
}
