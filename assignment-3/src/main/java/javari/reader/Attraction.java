package reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Attraction extends CsvReader{
	
	public Attraction(Path file) throws IOException {
		super(file);
    }
	
	public long countValidRecords(){
		long validRecords = 4;
		for (int i = 0; i < lines.size()-1; i++){
			String[] s = lines.get(i).split(COMMA);
			
			switch (s[1])
			{
				case "Circles of Fires": break;
				case "Dancing Animals": break;
				case "Counting Masters": break;
				case "Passionate Coders": break;
				default: validRecords -= 1; break;
			}
		}
		return validRecords;
	}
	
	public long countInvalidRecords(){
		long invalidRecords = 0;
		for (int i = 0; i < lines.size()-1; i++){
			String[] s = lines.get(i).split(",");
			switch (s[1])
			{
				case "Circles of Fires": break;
				case "Dancing Animals": break;
				case "Counting Masters": break;
				case "Passionate Coders": break;
				default: invalidRecords += 1; break;
			}
		}
		return invalidRecords;
	}
}