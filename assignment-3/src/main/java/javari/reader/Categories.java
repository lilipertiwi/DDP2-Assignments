package reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class Categories extends CsvReader{
	
	private String[] sections = {"Explore the Mammals","World of Aves","Reptillian Kingdom"};
	private String[] categories = {"Mammals","Aves","Reptiles"};
	public Categories(Path file) throws IOException {
		super(file);
    }
	
	public long countValidRecords(){
		long validRecords = 3;
		for (int i = 0; i < lines.size(); i++){
			String[] s = lines.get(i).split(COMMA);
			
			switch (s[2])
			{
				case "Explore the Mammals" : break;
				case "World of Aves": break;
				case "Reptillian Kingdom": break;
				default: validRecords -= 1; break;
			}
		}
		return validRecords;
	}
	
	public long countInvalidRecords(){
		long invalidRecords = 0;
		for (int i = 0; i < lines.size(); i++){
			String[] s = lines.get(i).split(",");
			switch (s[2])
			{
				case "Explore the Mammals" : break;
				case "World of Aves": break;
				case "Reptillian Kingdom": break;
				default: invalidRecords += 1; break;
			}
		}
		return invalidRecords;
	}
	
	public long validCategories(){
		long validRecords = 3;
		for (int i = 0; i < lines.size(); i++){
			String[] s = lines.get(i).split(COMMA);
			switch (s[1])
			{
				case "mammals" : break;
				case "aves": break;
				case "reptiles": break;
				default: validRecords -= 1; break;
			}
		}
		return validRecords;
	}
	
	public long invalidCategories(){
		long invalidRecords = 0;
		for (int i = 0; i < lines.size(); i++){
			String[] s = lines.get(i).split(",");
			switch (s[1])
			{
				case "mammals" : break;
				case "aves": break;
				case "reptiles": break;
				default: invalidRecords += 1; break;
			}
		}
		return invalidRecords;
	}
}