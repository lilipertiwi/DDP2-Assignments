package reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class AnimalRecord extends CsvReader{
	
	private String[] animals = {"Cat","Hamster","Whale","Parrot","Snake","Lion","Eagle"};
	public AnimalRecord(Path file) throws IOException {
		super(file);
    }
	
	public long countValidRecords(){
		long validRecords = 0;
		boolean typeIsValid;
		for (String s : lines){
			String[] ln = s.split(COMMA);		
			if (Arrays.asList(animals).contains(ln[1])){
				validRecords++;
			}
			
		}
		return validRecords;
	}
	
	public long countInvalidRecords(){
		long invalidRecords = 0;
		for (String s : lines){
			String[] ln = s.split(COMMA);
			if (!(Arrays.asList(animals).contains(ln[1]))){
				invalidRecords++;
			}
			
		}
		return invalidRecords;
	}
}