package park;
// import park.SelectedAttraction;
import java.util.List;
import java.util.ArrayList;
import animal.Animal;

public class Atraksi implements SelectedAttraction{
	private String name;
	private String type;
	private List<Animal> performers;
	
	public Atraksi(String name){
		this.name = name;
		this.performers = new ArrayList<Animal>();
	}
	
	public String getName(){
		return name;
	}
	
	public String getType(){
		return type;
	}
	
	public List<Animal> getPerformers(){
		return performers;
	}
	
	public boolean addPerformer(Animal performer){
		boolean isThere = false;
		
		for (Animal p : performers){
			if (p.equals(performer)){
				isThere = true;
			}
		}
		
		if (isThere == false){
			performers.add(performer);
			isThere = true;
		}
		return isThere;
	}
}