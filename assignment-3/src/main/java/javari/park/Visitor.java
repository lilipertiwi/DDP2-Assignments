package park;
// import park.Registration;
// import park.SelectedAttraction;
import java.util.List;
import java.util.ArrayList;
import animal.Animal;

public class Visitor implements Registration{
	private int registrationId;
	private String visitorName;
	private List<SelectedAttraction> selected;
	private String selectedAttractionName;
	private String animal;
	static ArrayList<Animal> performers;
	
	public Visitor(){
		this.registrationId = ++registrationId;
		selected = new ArrayList<SelectedAttraction>();
		performers = new ArrayList<Animal>();
	}
	
	public int getRegistrationId(){
		return registrationId;
	}
	
	public String getVisitorName(){
		return visitorName;
	}
	
	public String setVisitorName(String name){
		return this.visitorName = name;
	}
	
	public List<SelectedAttraction> getSelectedAttractions(){
		return selected;
	}
	
	public boolean addSelectedAttraction(SelectedAttraction attraction){
		selected.add(attraction);
		return true;
		
	}
}