
import reader.AnimalRecord;
import reader.Attraction;
import reader.Categories;
import reader.CsvReader;
import animal.Animal;
import animal.Gender;
import animal.Condition;
import animal.Mammals;
import animal.Aves;
import animal.Reptiles;
import park.Registration;
import park.SelectedAttraction;
import park.Visitor;
import park.Atraksi;
// import writer.RegistrationWriter;

import java.util.Arrays;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;



public class A3Festival{
	static Categories ctg;
	static Attraction atr;
	static AnimalRecord ar;
	static ArrayList<Animal> listAnimal = new ArrayList<Animal>();
	static ArrayList<SelectedAttraction> attractions = new ArrayList<SelectedAttraction>();
	static Visitor visitor = new Visitor();
	static String selectedType;
	
	public static void main(String[] args) throws IOException {
		
		
		
		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		
		Scanner in = new Scanner(System.in);
		
		//find file
		// try{
			System.out.println("... Loading... Success... System is populating data...");
			Path file1 = FileSystems.getDefault().getPath("D:\\ddp\\a3","animals_records.csv");
			Path file2 = FileSystems.getDefault().getPath("D:\\ddp\\a3","animals_attractions.csv");
			Path file3 = FileSystems.getDefault().getPath("D:\\ddp\\a3","animals_categories.csv");
		// }catch (FileNotFoundException e){
			// System.out.println("... Opening default section database from data. ... File not found or incorrect file!");
			// System.out.print("Please provide the source data path: ");
			// String path = in.nextLine();
			// Path file1 = FileSystems.getDefault().getPath(path,"animals_records.csv");
			// Path file2 = FileSystems.getDefault().getPath(path,"animals_attractions.csv");
			// Path file3 = FileSystems.getDefault().getPath(path,"animals_categories.csv");
		// }
		
		//save files as objects
		ctg = new Categories(file3);
		atr = new Attraction(file2);
		ar = new AnimalRecord(file1);
		
		//print files validation
		System.out.print("Found " + ctg.countValidRecords() + " valid sections and ");
		System.out.println(ctg.countInvalidRecords() + " invalid sections");
		System.out.print("Found " + atr.countValidRecords() + " valid attraction and ");
		System.out.println(atr.countInvalidRecords() + " invalid attractions");
		System.out.print("Found " + ctg.validCategories() + " valid categories and ");
		System.out.println(ctg.invalidCategories() + " invalid categories");
		System.out.print("Found " + ar.countValidRecords() + " valid animal records and ");
		System.out.println(ar.countInvalidRecords() + " invalid animal records");
		
		
		addAnimal(ar.getLines(), ctg.getLines());
		setAttraction(listAnimal, atr.getLines());
		canPerform(listAnimal.get(2));
		
		
		System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
		System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
		printSection();
		
		System.out.println("\nWow, one more step,");
		System.out.print("please let us know your name: ");
		String name = in.nextLine();
		visitor.setVisitorName(name);
		
		List<SelectedAttraction> a = visitor.getSelectedAttractions();
		
		System.out.println(a);
		System.out.println("\nYeay, final check! \nHere is your data, and the attraction you chose:");
		System.out.println("Name: " + visitor.getVisitorName());
		
			System.out.println("Attractions: " + a.get(0).getName() + " -> " + selectedType);
			for (Animal ab : listAnimal){
				if (selectedType.equals(ab.getType())){
					System.out.println("With: " + ab.getName());
				}
			}
		
		System.out.print("\nIs the data correct? (Y/N): ");
		System.out.print("\n\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
		// String output = RegistrationWriter.writeJson(visitor, "D:\\ddp\\a3");
		// RegistrationWriter.buildRegistration(output, visitor);
		// RegistrationWriter write = new RegistrationWriter(visitor, "D:\\ddp\\a3");
		// System.out.print("\n... End of program, write to registration_Dek_Depe.json ...");
    }
	
	
	//print the first menu (sections of animals)
	public static void printSection(){
		Scanner in = new Scanner(System.in);
		System.out.print("\nJavari Park has 3 sections:\n" + 
								"1. Explore the Mammals\n" + 
								"2. World of Aves\n" +
								"3. Reptilian Kingdom\n" +
								"Please choose your preferred section (type the number): ");
		String num = in.nextLine();
		sectionType(num);
	}
	
	//get types of animals from the chosen section
	public static void sectionType(String input ){
		Scanner in = new Scanner(System.in);
		ArrayList<String> list = new ArrayList<String>();	//list daftar tipe animal sesuai section yang dipilih
		int number = 1;
		if (input.equals("1")){
			System.out.println("\n--World of Mammals--");
			for (String s : ctg.getLines()){
				String[] val = s.split(",");
				if (val[1].equalsIgnoreCase("mammals")){
					list.add(val[0]);
					System.out.println(number + ". " + val[0]);
					number++;
				}

			}
		}else if (input.equals("2")){
			System.out.println("\n--World of Aves--");
			for (String s : ctg.getLines()){
				String[] val = s.split(",");
				if (val[1].equalsIgnoreCase("aves")){
					list.add(val[0]);
					System.out.println(number + ". " + val[0]);
					number++;
				}
				
			}
		}else if (input.equals("3")){
			System.out.println("\n--World of Reptiles--");
			for (String s : ctg.getLines()){
				String[] val = s.split(",");
				if (val[1].equalsIgnoreCase("reptiles")){
						list.add(val[0]);
					System.out.println(number + ". " + val[0]);
					number++;
				}	
			}
		}else if (input.equals("#")){
			printSection();
		}else{
			System.out.print("\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu\n");
			printSection();
		}
		
		
		System.out.print("Please choose your preferred animal (type the number): ");
		String num = in.nextLine();
		if (num.equals("#")){
			printSection();
		}else {
			chooseAttraction(num, list);
		}
	}
	
	public static void chooseAttraction(String num, ArrayList<String> list){
		Scanner in = new Scanner(System.in);
		System.out.println();
		int countList = 0;
		int nums = 0;
		int index = Integer.parseInt(num);
		selectedType = list.get(index-1);
		ArrayList<String> atraksi = new ArrayList<String>();	//list atraksi
		for (Animal a : listAnimal){
			if (a.getType().equals(list.get(index-1)) && canPerform(a) == true){
				atraksi = a.getAttractions();
				nums++;
			}
		}
		if (nums <= 0){
			System.out.println("Unfortunately, no " + list.get(index-1) + " can perform any attraction, please choose other animals");
		}else {
			System.out.println("index : " + index);
			System.out.println("--"+list.get(index-1)+"--");
			for (String s : atraksi){
				System.out.println(++countList + ". " + s);
			}
			System.out.print("Please choose your preferred attractions (type the number): ");
			int input = Integer.parseInt(in.nextLine());
			for (SelectedAttraction atr : attractions){
				if (atr.getName().equals(atraksi.get(input-1))){
					visitor.addSelectedAttraction(atr);
				}
			}
		}
	}
	

	//method to instance and add Animals from animals_records
	public static void addAnimal(List<String> records, List<String> category){
		for (int i = 0; i < records.size(); i++){
			for (int j = 0; j < category.size(); j++){
				String[] attr = records.get(i).split(",");
				Integer id = Integer.valueOf(attr[0]);
				String animal = attr[1];
				String name = attr[2];
				Gender gender = Gender.parseGender(attr[3]);
				double length = Double.parseDouble(attr[4]);
				double weight = Double.parseDouble(attr[5]);
				String spStatus = attr[6];
				Condition cond = Condition.parseCondition(attr[7]);
				
				String[] sec = category.get(j).split(",");
				if (attr[1].equalsIgnoreCase(sec[0])){
					if (sec[1].equalsIgnoreCase("mammals")){
						Mammals anm = new Mammals(id, animal, name, gender, length, weight, spStatus, cond);
						listAnimal.add(anm);
					}else if (sec[1].equalsIgnoreCase("aves")){
						Aves anm = new Aves(id, animal, name, gender, length, weight, spStatus, cond);
						listAnimal.add(anm);
					}else if (sec[1].equalsIgnoreCase("reptiles")){
						Reptiles anm = new Reptiles(id, animal, name, gender, length, weight, spStatus, cond);
						listAnimal.add(anm);
					}
				}
			}
			
		}
	}
	
	
	
	//specifying attractions that can be performed by chosen animal
	public static void setAttraction(ArrayList<Animal> listAnimal, List<String> atr){
		for (String ln : atr){
			String[] s = ln.split(",");
			for (Animal a : listAnimal){
				if (s[0].equalsIgnoreCase(a.getType())){
						a.addAttraction(s[1]);
				}
			}
		}
		
		for (String lines : atr){
			boolean isThere = false;
			String[] s = lines.split(",");
			for (SelectedAttraction atraksi : attractions){
				if (atraksi.getName().equals(s[1])){
					isThere = true;
				}
			}
			if(isThere == false){
				SelectedAttraction attr = new Atraksi(s[1]);
				attractions.add(attr);
				for (Animal a : listAnimal){
					if (s[0].equalsIgnoreCase(a.getType())){
						attr.addPerformer(a);
					}	
				}
			}
			
		}
	}
	
	
	//check if animal can perform or not using its specificCondition
	public static boolean canPerform(Animal animal){
		if (animal instanceof Mammals){
			Mammals m = (Mammals) animal;
			if (m.specificCondition() == true){
				return true;
			}
		}else if (animal instanceof Aves){
			Aves a = (Aves) animal;
			if (a.specificCondition() == true){
				return true;
			}
		}else if (animal instanceof Reptiles){
			Reptiles r = (Reptiles) animal;
			if (r.specificCondition() == true){
				return true;
			}
		}return false;
	}
	
}