package animal;

public class Aves extends Animal{
	private String sectionName = "World of Aves";
	private boolean isLayingEggs;
	
	public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String status, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
		isLayingEggs(status);
    }
	
	public String getSectionName(){
		return sectionName;
	}
	
	public boolean isLayingEggs(String status){
		if (status.equalsIgnoreCase("laying eggs")){
			isLayingEggs = true;
		}
		return isLayingEggs;
	}
	
	public boolean specificCondition(){
		if (isLayingEggs == true){
			return false;
		}
		return true;
	}
}