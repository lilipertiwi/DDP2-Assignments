package animal;

public class Mammals extends Animal{
	private String sectionName = "Explore the Mammals";
	private boolean isPregnant;
	
	public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, String status, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
		isPregnant(status);
    }
	
	public String getSectionName(){
		return sectionName;
	}
	
	public boolean isPregnant(String status){
		if (status.equalsIgnoreCase("pregnant")){
			isPregnant = true;
		}
		return isPregnant;
	}
	
	public boolean specificCondition(){
		if (isPregnant == true){
			return false;
		}
		return true;
	}
}