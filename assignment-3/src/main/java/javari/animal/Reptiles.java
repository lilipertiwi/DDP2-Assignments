package animal;

public class Reptiles extends Animal{
	private String sectionName = "Reptilian Kingdom";
	private boolean isTame;
	
	public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, String status, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
		isTame(status);
    }
	
	public String getSectionName(){
		return sectionName;
	}
	
	public boolean isTame(String status){
		if (status.equalsIgnoreCase("tame")){
			isTame = true;
		}
		return isTame;
	}
	
	public boolean specificCondition(){
		if (isTame == true){
			return true;
		}
		return false;
	}
}