package javariGame;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Board extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private static final int NUMBER_OF_ROWS = 6;
	private static final int NUMBER_OF_COLUMNS = 6;
	private static final int NUM_OF_PAIRS = 18;
	private static final int NUM_OF_CARDS = 36;
	
	private int numClicks;
	private ImageIcon cover = new ImageIcon(getClass().getResource("/images/cover.jpg"));
	private ImageIcon blank = new ImageIcon(getClass().getResource("/images/empty.jpg"));
	private Card[][] card = null;
	private String[] cardStorage = randomImage();
	
	private static int numOfFailedAttempts = 0;
	
	private Card card1;
	private Card card2;
	
	
	
	/**
	 * A constructor to initialize the Board of Cards to be used for the game
	 */
	public Board() {
		super();
		
		setLayout(new GridLayout (6, 6));
		
		cover = new ImageIcon(getClass().getResource("/images/cover.jpg"));
		
		card = new Card[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				card[row][column] = new Card(0);
				card[row][column].addActionListener(this);
				add(card[row][column]);
			}
		}
        start();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#getPreferredSize()
	 * to set the screen size of the game.
	 */
	public Dimension getPreferredSize() {
        return new Dimension(600, 650);
    }
	
	/**
	 * Method to replay the game. Reinitialize the board with current set of cards.
	 */
	public void reStart() {
		resetMatchedImages();
		resetBoard();
		setImages();
		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				card[row][column].setIcon(cover);
			}
		}
	}
	
	/**
	 * Method to start the game. Preparing the board with set of cards.
	 */
	public void start() {
		resetMatchedImages();//
		resetBoard();
		cardStorage = randomImage();
		setImages();
		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				card[row][column].setIcon(cover);
			}
		}
	}
	
	
	/**
	 * To reset all the matched image, so the game could be replayed or started.
	 */
	public void resetMatchedImages() {
		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				if (card[row][column].isMatched()) {
					card[row][column].setMatched(false);
				}
			}
		}
	}

	/**
	 * Reset the state of Failed attempts and matched cards.
	 */
	public void resetBoard() {
		numOfFailedAttempts = 0;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * this method runs each time user clicks a button.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		++numClicks;
		try {
			if ((card1 != null && card1.isMatched()) &&
					(card2 != null && card2.isMatched())) {
//			if (card1.isMatched() && card2.isMatched()) {
				showImage(card1);
				showImage(card2);
//			}
			}
		}catch (NullPointerException e1) {
			System.out.println("");
		}
				
		if (numClicks < 2) {
			card1 = (Card) e.getSource();
			showImage(card1);
			card1.setSelected(true);
			showCards(); //unmatched cards will be covered again
		}else if (numClicks == 2) {
			card2 = (Card) e.getSource();
			showImage(card2);
			card2.setSelected(true);
			if ((card1.getLocation() != card2.getLocation()) &&
					(card1.getCardType() == card2.getCardType())){
				card1.setMatched(true);
				card2.setMatched(true);
				card1.setCardType(0);
				card2.setCardType(0);
				isCompleted();
			}else{
				numOfFailedAttempts += 1;
				card1.setSelected(false);
				card2.setSelected(false);
			}
			numClicks = 0;
		}
	}
	
	/**
	 * Check if all cards already opened.
	 */
	public void isCompleted() {
		for (int i = 0; i < NUMBER_OF_ROWS; i++) {
			for (int j = 0; j < NUMBER_OF_COLUMNS; j++) {
				if(!card[i][j].isBlank()) {
					return;
				}
			}
		}
		System.out.println("Well done!");
		resetMatchedImages();
	}
	
	/**
	 * This method show all cards during the game.
	 */
	public void showCards() {
		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				// Is card selected ?
				if (!card[row][column].isSelected()) {
					// If selected, verify if the card was matched by the user
					if (card[row][column].isMatched()) {
						// It was matched, empty the card slot
						card[row][column].setIcon(blank);
					} else {
						// It was not, put the "hidden card" image
						card[row][column].setIcon(cover);
					}
				}
			} 
		}
	}
	
	/**
	 * This method shows image of selected card. Also check if the card
	 * should be blank(matched card) or not.
	 */
	private void showImage(Card card) {
		if(!card.isMatched()) {
		  card.setIcon(card.getImage());
		}else {
			card.setImage(card.getBlankImage());
			card.setIcon(card.getImage());
		}
	}
	
	/**
	 * This method sets the image of each card.
	 */
	private void setImages() {
		ImageIcon anImage;

		for (int row = 0; row < NUMBER_OF_ROWS; row++) {
			for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
				if (!card[row][column].isMatched()) {
				URL file = getClass().getResource("/images/img-" + cardStorage[column
		          + (NUMBER_OF_COLUMNS * row)] + ".jpg");

				anImage = new ImageIcon(file);
				String type = cardStorage[column + (NUMBER_OF_COLUMNS * row)];
				int parsedType = Integer.parseInt(type);

				card[row][column].setCardType(parsedType);
				card[row][column].setImage(anImage);
				}
			}
		}
	}

	/**
	 * This method generate an array of String contains the 18 paired images random.
	 * @return Array of string of images file numbers.
	 */
	public String[] randomImage() {
		String[] firstSet = new String[NUM_OF_PAIRS];
		String[] secondSet = new String[NUM_OF_PAIRS];
		String[] storage = new String[NUM_OF_CARDS];
		
		ArrayList<String> generated = new ArrayList<String>();
		for (int i = 0; i < NUM_OF_PAIRS; i++) {
			while (true){
				Random random = new Random();
				Integer number = 1 + random.nextInt(18);
				String fileNumber = number.toString();
				
				if (!generated.contains(fileNumber)) {
					generated.add(fileNumber);
					firstSet[i] = generated.get(i);
					break;
				}
			}
		}
		
		for (int i = 0; i < NUM_OF_PAIRS; i++) {
			storage[i] = firstSet[i];
		}
		Collections.shuffle(Arrays.asList(firstSet));
		for (int j = 0; j < NUM_OF_PAIRS; j++) {
			secondSet[j] = firstSet[j];
		}
		for (int k = NUM_OF_PAIRS; k < NUM_OF_CARDS; k++) {
			storage[k] = secondSet[k - NUM_OF_PAIRS];
		}
		Collections.shuffle(Arrays.asList(storage));
		return storage;
	}
	
	public int getFailedAttempts() {
		return numOfFailedAttempts;
	}
}
