package javariGame;

import javax.swing.ImageIcon;

import javax.swing.JButton;

/**
 * @author Lili Pertiwi
 *
 */
public class Card extends JButton{

	private static final long serialVersionUID = 1L;
	private final int blankType = 0;
	private int cardType;
	private boolean isMatched = false;
	private boolean isSelected = false;
	private ImageIcon image;
	private ImageIcon blankImage = new ImageIcon(getClass().getResource("/images/empty.jpg"));
	
	/**
	 * Constructor of a card.
	 * @param type as the identity of the card.
	 */
	public Card(int type) {
		super();
		setCardType(type);
	}

	/**
	 * return type of the card.
	 * @return card's type
	 */
	public int getCardType() {
		return cardType;
	}

	/**
	 * To change the card's type to another type.
	 * @param cardType as the new type
	 */
	public void setCardType(int cardType) {
		this.cardType = cardType;
	}
	
	/**
	 * Check if the pair of a card is already found.
	 * @return true if it's matched. false if it's not.
	 */
	public boolean isMatched() {
		if (isMatched == true) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check if the card have been paired with another card 
	 * (the card then should display white blank image).
	 * @return true if it's blank.
	 */
	public boolean isBlank() {
		if (this.getCardType() == blankType) {
			return true;
		}
		return false;
	}
	
	/**
	 * To change the card's match status
	 * @param set as the new match status
	 */
	public void setMatched(boolean set) {
		isMatched = set;
	}
	
	/**To check if the card already been selected.
	 * @see javax.swing.AbstractButton#isSelected()
	 */
	public boolean isSelected() {
		if (isSelected == true) {
			return true;
		}
		return false;
	}
	
	/** (non-Javadoc)
	 * change the selected status.
	 * @see javax.swing.AbstractButton#setSelected(boolean)
	 */
	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	/**To get the image of the card.
	 * @return card's image
	 */
	public ImageIcon getImage() {
		return image;
	}

	/**To change card's image with new image.
	 * @param image as the new image to be applied
	 */
	public void setImage(ImageIcon image) {
		this.image = image;
	}

	/**
	 * To use the white blank image when the card already paired.
	 * @return blank image
	 */
	public ImageIcon getBlankImage() {
		return blankImage;
	}

	/**
	 * To get the blank type of the card.
	 * @return blank type
	 */
	public int getBlankType() {
		return blankType;
	}
}
