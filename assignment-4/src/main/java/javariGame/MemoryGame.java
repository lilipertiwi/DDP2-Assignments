package javariGame;

import java.lang.String;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class MemoryGame extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private Board cards;
	private JButton retryButton;
	private JButton newButton;
	private JPanel attempts;
	private JSplitPane splitPane;
	
	/**
	 * Initialize the MemoryGame class.
	 */
	public MemoryGame() {
		super("Javari Memory Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		cards = new Board();
		add(cards, BorderLayout.CENTER);
		
		attempts = new JPanel();
		attempts.setLayout(new FlowLayout());
		attempts.add(new JLabel("Attempts: " + cards.getFailedAttempts()));
		add(attempts, BorderLayout.NORTH);
		
		splitPane = new JSplitPane();
		add(splitPane, BorderLayout.SOUTH);

		retryButton = new JButton("Retry");
		retryButton.setFocusPainted(false);
		retryButton.addMouseListener(btnMouseListener);
		splitPane.setLeftComponent(retryButton);

		newButton = new JButton("New Game");
		newButton.setFocusPainted(false);
		newButton.addMouseListener(btnMouseListener);
		splitPane.setRightComponent(newButton);
		
		pack();
		setResizable(true);
		setVisible(true);
	}
	
	/**
	 * An anonymous class for receiving input when the mouse is clicked.
	 */
	private MouseListener btnMouseListener = new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 1 && e.getComponent() == retryButton) {
				cards.reStart();
			} else if (e.getClickCount() == 1 && e.getComponent() == newButton) {
				cards.start();
			}
		}
	};

	/**
	 * Main method to start this Matching-card Game
	 * @param args as the parameter
	 */
	public static void main(String[] args) {
		  new MemoryGame();
	}
}
